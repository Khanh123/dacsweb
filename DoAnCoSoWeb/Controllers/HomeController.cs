﻿using DoAnCoSoWeb.Models;
using DoAnCoSoWeb.Repository;
using DoAnCoSoWeb.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Security.Claims;

namespace DoAnCoSoWeb.Controllers
{
    public class HomeController : Controller
    {

        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(string sortOrder, decimal? minPrice, decimal? maxPrice, int[] hangIds)
        {
            var sanphams = _context.sanphams.AsQueryable();
            var hangs = await _context.hangs.ToListAsync();

            // Lọc sản phẩm theo hãng
            if (hangIds != null && hangIds.Any())
            {
                sanphams = sanphams.Where(s => hangIds.Contains(s.HangId));
            }

            // Lọc sản phẩm theo khoảng giá
            if (minPrice.HasValue)
            {
                sanphams = sanphams.Where(s => s.Gia >= minPrice.Value);
            }

            if (maxPrice.HasValue)
            {
                sanphams = sanphams.Where(s => s.Gia <= maxPrice.Value);
            }

            // Sắp xếp sản phẩm
            switch (sortOrder)
            {
                case "lowToHigh":
                    sanphams = sanphams.OrderBy(s => s.Gia);
                    break;
                case "highToLow":
                    sanphams = sanphams.OrderByDescending(s => s.Gia);
                    break;
                default:
                    // Mặc định không sắp xếp
                    break;
            }

            // Truy vấn cơ sở dữ liệu để lấy thông tin số lượng đã bán của mỗi sản phẩm
            var soldQuantities = await _context.chitiethoadons
                .GroupBy(c => c.SanphamId)
                .Select(g => new
                {
                    SanPhamId = g.Key,
                    SoLuongDaBan = g.Sum(x => x.SoLuong)
                })
                .ToDictionaryAsync(x => x.SanPhamId, x => x.SoLuongDaBan);

            var viewModel = new HomeViewModels
            {
                hangs = hangs,
                sanphams = await sanphams.ToListAsync(),
                soldQuantities = soldQuantities,
                MinPrice = minPrice,
                MaxPrice = maxPrice,
                SelectedHangIds = hangIds // Truyền lại hangIds cho view
            };

            return View(viewModel);
        }
        
        public IActionResult MostSold()
        {
            var mostSoldProducts = _context.chitiethoadons
                .GroupBy(c => c.SanphamId)
                .Select(g => new
                {
                    SanPhamId = g.Key,
                    SoLuongMua = g.Sum(x => x.SoLuong)
                })
                .OrderByDescending(x => x.SoLuongMua)
                .ToList();

            var mostSoldProductIds = mostSoldProducts.Select(x => x.SanPhamId).ToList();

            var sanphams = _context.sanphams
                .Where(s => mostSoldProductIds.Contains(s.Id))
                .ToList();

            var sortedSanphams = sanphams
                .OrderByDescending(s => mostSoldProductIds.IndexOf(s.Id))
                .ToList();

            // Sửa đổi phần gán dữ liệu cho thuộc tính soldQuantities
            var viewModel = new HomeViewModels
            {
                sanphams = sortedSanphams,
                soldQuantities = mostSoldProducts.ToDictionary(x => x.SanPhamId, x => x.SoLuongMua)
            };

            return View(viewModel);
        }


        [HttpGet]
        public IActionResult IsLoggedIn()
        {
            var status = User.Identity.IsAuthenticated;
            return Json(new { status = status });
        }

        public async Task<IActionResult> ProdDetail(long id, int page = 1)
        {
            if (id <= 0)
            {
                return RedirectToAction("Index", "Error");
            }

            var product = await _context.sanphams.FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                var errorViewModel = new ErrorViewModel
                {
                    RequestId = "Product Error",
                };
                return View("Error", errorViewModel);
            }

            int pageSize = 10;
            var comments = await _context.comments
                .Where(c => c.SanphamId == id)
                .Include(c => c.Account)
                .Include(c => c.Tinhtrangcomment) // Bổ sung include cho thông tin trạng thái bình luận
                .OrderByDescending(c => c.NgayComment)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            var totalComments = await _context.comments.CountAsync(c => c.SanphamId == id);

            var viewModel = new HomeViewModels
            {
                sanphams = new List<Sanpham> { product },
                Comments = comments,
                CurrentPage = page,
                TotalPages = (int)Math.Ceiling(totalComments / (double)pageSize)
            };

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(int productId, string note)
        {
            var accountId = HttpContext.Session.GetInt32("AccountId");
            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized();
            }

            var userBoughtProduct = await _context.chitiethoadons
                .AnyAsync(hd => hd.AccountId == accountId && hd.SanphamId == productId);

            var commentTinhTrangId = userBoughtProduct ? 1 : 2;

            var comment = new Comment
            {
                AccountId = accountId.Value,
                NgayComment = DateTime.Now,
                Note = note,
                SanphamId = productId,
                TinhtrangcommentId = commentTinhTrangId // Thay đổi thành TinhtrangcommentId để tham chiếu đến Id của Tinhtrangcomment
            };

            _context.comments.Add(comment);
            await _context.SaveChangesAsync();

            return Json(new { success = true });
        }


        [HttpGet]
        public async Task<IActionResult> _Search(string query, int page = 1)
        {
            if (string.IsNullOrEmpty(query))
            {
                return RedirectToAction("Index");
            }

            int pageSize = 10;

            var products = await _context.sanphams
                .Where(p => p.TenSanPham.Contains(query))
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            var totalProducts = await _context.sanphams
                .Where(p => p.TenSanPham.Contains(query))
                .CountAsync();

            var viewModel = new HomeViewModels
            {
                sanphams = products,
                CurrentPage = page,
                TotalPages = (int)Math.Ceiling(totalProducts / (double)pageSize),
                Query = query // Truyền query vào viewModel để hiển thị lại trên view Search khi cần
            };

            return View("SearchResult", viewModel); // Sử dụng view SearchResult để hiển thị kết quả tìm kiếm
        }


        [HttpGet]
        public IActionResult SearchResult()
        {
            return View();
        }

        public async Task<IActionResult> RamProduct()
        {
            var rams = await _context.sanphams.Where(m => m.LoaiId == 1).ToListAsync();

            var viewModel = new HomeViewModels
            {
                ram = rams,
            };
            return View(viewModel);
        }
    }
}
